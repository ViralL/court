$(document).ready(function(){function a(){var a=$(window).height();$(".full-screen").css("height",a)}$("#steps").steps({headerTag:"h3",bodyTag:"section",transitionEffect:"fade",stepsOrientation:"vertical",labels:{cancel:"Отменить",finish:"Завершить",next:"Далее",previous:"Назад",loading:"Загрузка ..."}}),$("select").select2({minimumResultsForSearch:1/0});var b={data:[{name:"Наши преимущества"},{name:"Структура Третейского суда НАП"},{name:"Учредители"},{name:"Статистика"},{name:"О нас"}],getValue:"name",list:{match:{enabled:!0}},theme:"plate-dark"};$("[name='search']").easyAutocomplete(b),$(".slide-calendar").carousel({interval:!1}),a(),$(window).bind("resize",a);var c=Math.max(document.documentElement.clientWidth,window.innerWidth||0);if(c>1023){var d=$(".onepagescroll");d.length&&d.onepage_scroll({sectionContainer:"section",easing:"ease",animationTime:2e3,pagination:!0,updateURL:!1,afterMove:function(a){}})}else $(".navbar-toggle").click(function(){$(".sidebar-navbar-collapse").slideToggle("slow")}),$("main").click(function(){$(".sidebar-navbar-collapse").hide()}),$(".sidebar-nav__main").click(function(){$(this).toggleClass("active")});$(".carousel-indicators li").click(function(){$(this).attr("data-width")})}),document.body.onload=function(){function a(){var a=1;"calcs"==$("input:checked").attr("id")&&(a=2),b(a)}function b(a){var b=0;$('[id ^= "matdem_"]').each(function(d){""!=$(this).val()&&($("#matdemsum_"+$(this).attr("id").substr(-2)).html(c($(this).val(),a)),b+=parseFloat($(this).val()))});var j=d(a),k=e(j,a);f(j,k),g(j,b),h(j+k,a),i(b)}function c(a,b){var c=0;return a=a.replace(",","."),a=a.replace(/\s+/g,""),a=parseFloat(a),0==a?c:(c=.01*a,1==b?(a>=12e5&&a<2e6&&(c=23e3+.01*(a-1e6)),a>=2e6&&a<=46e5&&(c=33e3+.005*(a-2e6)),a>46e5&&.01*a>195e3&&(c=195e3)):(a>4e5&&a<1e6&&(c=5200+.01*(a-2e5)),a>=1e6&&a<=16e5&&(c=13200+.005*(a-1e6)),a>16e5&&.01*a>29e3&&(c=29e3)),j(c))}function d(a){var b=25e3;2==a&&(b=7e3);var c=0;return $('[id ^= "matdemsum_"]').each(function(a){c+=parseFloat($(this).html())}),c>0&&c<b&&(c=b),$("#matdemsum").html(c),c}function e(a,b){var c=4e3,d=25e3;2==b&&(c=2e3,d=7e3);var e=0,f=parseInt($("#matnotdem").val(),10);return e=0==a&&0==f?0:0==a&&f*c<d?d:f*c,$("#matnotdemsum").html(e),e}function f(a,b){var c=a+b;$("#fullsum").html(c)}function g(a,b){var c=parseFloat($("#sumalldem").val()),d=c*a/b,e=a-d;$("#sumplaintiff").html(d),$("#sumappellee").html(e)}function h(a,b){$("#percent01").html(.9*a),$("#percent02").html(.9*a),$("#percent03").html(.8*a),$("#percent04").html(.5*a),2==b&&$("#percent04").html(.7*a),$("#percent05").html(.5*a)}function i(a){var b=parseFloat($("#sumalldem").val());b>a?($("#alertdanger").css("display","inline-block"),$("#alertsuccess").css("display","none")):($("#alertdanger").css("display","none"),$("#alertsuccess").css("display","inline-block"))}function j(a){return a=Math.round(a)}function k(){var a=$("#matnotdem"),b=parseInt(a.val())-1;return b=b<0?0:b,a.val(b),a.change(),!1}function l(){var a=$("#matnotdem");return a.val(parseInt(a.val())+1),a.change(),!1}function m(a){$num=parseInt($('[id ^= "matdem_"]:last').attr("id").substr(-2),10)+1,$('<li class="form-group"><div class="input-group"><input type="text" class="form-control" id="matdem_'+n($num)+'"><span class="input-group-addon text-lg"> <i>r</i> </span></div></li>').insertBefore("#insertbefore1"),$('<div class="form-group"><span id="matdemsum_'+n($num)+'">0,00</span> <i>r</i></div>').insertBefore("#insertbefore2")}function n(a){return(a<10?"0":"")+a}$("#minus").click(k),$("#plus").click(l),$("#addNewMatdem").click(m),$("#calculator").length&&(document.forms.calculator.onchange=a,a())};
//
//function drawDottedPath(path,_options){
//    var defaultOptions={
//        time: 5000,
//        loop: false
//    };
//
//    var options={};
//    for (var option in defaultOptions) { options[option] = defaultOptions[option]}
//    for (var option in _options) { options[option] = _options[option]}
//
//    var currentStep=0;
//    var animInterval;
//
//    var pathLength=path.getTotalLength();
//    var originalDasharray=getComputedStyle(path).strokeDasharray;
//    var dashLength=parseFloat(originalDasharray.split(",")[0]);
//    var dashPadding=parseFloat(originalDasharray.split(",")[1]);
//    var numDashes=Math.ceil(pathLength/(dashLength+dashPadding));
//    var dashArrayArray=[];
//    for (var x=0;x<numDashes;x++){
//        if (x==0){
//            dashArrayArray[x]="0 "+dashLength;
//        }
//        else{
//            dashArrayArray[x]="";
//            for (var y=0;y<x;y++){
//                if(y<x-1){
//                    dashArrayArray[x]+=dashLength+" "+dashPadding+" ";
//                }
//                else{
//                    dashArrayArray[x]+=dashLength+" ";
//                }
//            }
//            dashArrayArray[x]+=(pathLength-y*(dashLength+dashPadding));
//        }
//    }
//    animInterval=setInterval(animStep,options.time/numDashes);
//
//
//    function animStep(){
//        if (currentStep<numDashes){
//            path.style["stroke-dasharray"]=dashArrayArray[currentStep];
//            currentStep++;
//        }
//        else{
//            currentStep=0;
//            if (!options.loop){
//                clearInterval(animInterval);
//                if (options.callback){
//                    options.callback();
//                }
//            }
//        }
//    }
//};
//
//var path=document.body.querySelector(".dottedPath");
//drawDottedPath(path,{loop: true});