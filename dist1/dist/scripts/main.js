

$(document).ready(function() {


    //popup
    //$(function () {
    //    $('.popup-modal').magnificPopup({
    //        type: 'inline',
    //        modal: true
    //    });
    //    $(document).on('click', '.popup-modal-dismiss', function (e) {
    //        e.preventDefault();
    //        $.magnificPopup.close();
    //    });
    //});
    //popup






    // steps
    $("#steps").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        stepsOrientation: "vertical",
        labels: {
            cancel: "Отменить",
            finish: "Завершить",
            next: "Далее",
            previous: "Назад",
            loading: "Загрузка ..."
        }
    });
    // steps


    //custom select
    $("select").select2({
        minimumResultsForSearch: Infinity
    });
    //custom select


    var options = {
        data: [
            {
                "name": "Наши преимущества"
            },
            {
                "name": "Структура Третейского суда НАП"
            },
            {
                "name": "Учредители"
            },
            {
                "name": "Статистика"
            },
            {
                "name": "О нас"
            }
        ],

        getValue: "name",

        list: {
            match: {
                enabled: true
            }
        },

        theme: "plate-dark"
    };
    $("[name='search']").easyAutocomplete(options);



    $('.slide-calendar').carousel({
        interval: false
    });
    //$('#proceed').carousel({
    //    interval: false
    //});


    // screen height
    function alturaMaxima() {
        var altura = $(window).height();
        $(".full-screen").css('height', altura);
    }
    alturaMaxima();
    $(window).bind('resize', alturaMaxima);
    // screen height
    var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (screen_width > 1023) {

        //fullpagescroll index
        var pageScroll = $('.onepagescroll');
        if (pageScroll.length) {
            pageScroll.onepage_scroll({
                sectionContainer: "section",
                easing: "ease",
                animationTime: 2000,
                pagination: true,
                //loop: false,
                updateURL: false,
                //direction: "horizontal"
                afterMove: function(index) {
                    // slider
                    //$('.better').slick({
                    //    dots: false,
                    //    infinite: false
                    //});
                    // slider
                }
            });
        }
        //$('.onepage-prev').click(function () {
        //    $(pageScroll).moveUp();
        //});
        //$('.onepage-next').click(function () {
        //    $(pageScroll).moveDown();
        //});
        //fullpagescroll index

    } else {

        // adaptive menu
        $('.navbar-toggle').click(function () {
            $('.sidebar-navbar-collapse').slideToggle('slow');
        });
        $('main').click(function () {
            $('.sidebar-navbar-collapse').hide();
        });
        $('.sidebar-nav__main').click(function () {
            $(this).toggleClass('active');
        });
        // adaptive menu
    }

    $('.carousel-indicators li').click(function(){
        var widthL = $(this).attr('data-width');
        //$(this).parent().find('.active-dot').css('width', widthL);
        //$(this).animate({ num: dataN}, {
        //    duration: 800,
        //    step: function (num){
        //        this.innerHTML = (num).toFixed(0)
        //    }
        //});
    });


});


document.body.onload = function(){

    function calc(){
        var checkbox = 1;
        if ($("input:checked").attr('id')=='calcs') checkbox = 2;
        calculator(checkbox);
    }

    function calculator(checkbox){
        var sum = 0;
        $('[id ^= "matdem_"]').each(function( index ) {
            if ($( this ).val() != '') {
                $('#matdemsum_' + $( this ).attr('id').substr(-2)).html(matdem($( this ).val(), checkbox));
                sum += parseFloat($( this ).val());
            }
        });
        var mds = matdemsum(checkbox);
        var mnds = matnotdemsum(mds, checkbox);

        fullsum(mds, mnds);

        distribution(mds, sum);

        percents(mds+mnds, checkbox);

        showalert(sum);
    };

    function matdem(inp, checkbox){
        var val = 0;
        inp = inp.replace(",", '.');
        inp = inp.replace(/\s+/g, '');
        inp = parseFloat(inp);
        if (inp == 0) {
            return val;
        }
        val = inp*0.01;
        if (checkbox == 1) {
            if (inp >= 1200000 && inp <2000000)
                val = 23000 + (inp-1000000)*0.01;
            if (inp >= 2000000 && inp <=4600000)
                val = 33000 + (inp-2000000)*0.005;
            if (inp > 4600000 && (inp*0.01)>195000)
                val = 195000;
        }
        else{
            if (inp > 400000 && inp <1000000)
                val = 5200 + (inp-200000)*0.01;
            if (inp >= 1000000 && inp <=1600000)
                val = 13200 + (inp-1000000)*0.005;
            if (inp > 1600000 && (inp*0.01)>29000)
                val = 29000;
        }
        return digitsFormat(val);
    }

    function matdemsum(checkbox){
        var checkval = 25000;
        if (checkbox == 2) checkval = 7000;
        var sum = 0;
        $('[id ^= "matdemsum_"]').each(function( index ) {
            sum += parseFloat($( this ).html());
        });
        if (sum > 0 && sum < checkval) sum = checkval;
        $('#matdemsum').html(sum);
        return sum;
    }

    function matnotdemsum(mds, checkbox){
        // неимущественные требования
        var checkval1 = 4000;
        var checkval2 = 25000;
        if (checkbox == 2) {
            checkval1 = 2000;
            checkval2 = 7000;
        }
        var matnotdemsum = 0;
        var matnotdem = parseInt($('#matnotdem').val(), 10);
        if (mds == 0 && matnotdem == 0) matnotdemsum = 0
        else{
            if (mds == 0 && matnotdem*checkval1 < checkval2) matnotdemsum = checkval2
            else matnotdemsum = matnotdem*checkval1;
        }
        $('#matnotdemsum').html(matnotdemsum);
        return matnotdemsum;
    }

    function fullsum(mds, mnds){
        var fullsum = mds + mnds;
        $('#fullsum').html(fullsum);
    }

    function distribution(mds, sum) {
        var sumalldem = parseFloat($('#sumalldem').val());
        var summds = 0;
        var sumplaintiff = sumalldem*mds/sum;
        var sumappellee = mds - sumplaintiff;
        $('#sumplaintiff').html(sumplaintiff);
        $('#sumappellee').html(sumappellee);
    }

    function percents(fullsum, checkbox){
        $('#percent01').html(fullsum*0.9);
        $('#percent02').html(fullsum*0.9);
        $('#percent03').html(fullsum*0.8);
        $('#percent04').html(fullsum*0.5);
        if (checkbox == 2)$('#percent04').html(fullsum*0.7);
        $('#percent05').html(fullsum*0.5);
    }

    function showalert(sum){
        var checkval = parseFloat($('#sumalldem').val());
        if (checkval > sum) {
            $('#alertdanger').css("display", "inline-block");
            $('#alertsuccess').css("display", "none");
        }
        else{
            $('#alertdanger').css("display", "none");
            $('#alertsuccess').css("display", "inline-block");

        }
    }

    function digitsFormat(str) {
        str = Math.round(str);
        return str;
    }

    function minus () {
        var $input = $('#matnotdem');
        var count = parseInt($input.val()) - 1;
        count = count < 0 ? 0 : count;
        $input.val(count);
        $input.change();
        return false;
    }

    function plus() {
        var $input = $('#matnotdem');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    }

    function addNewMatdem(args) {
        $num = parseInt($('[id ^= "matdem_"]:last').attr('id').substr(-2), 10) + 1;
        $('<li class="form-group"><div class="input-group"><input type="text" class="form-control" id="matdem_' + leadZero($num) + '"><span class="input-group-addon text-lg"> <i>r</i> </span></div></li>').insertBefore('#insertbefore1');
        $('<div class="form-group"><span id="matdemsum_' + leadZero($num) + '">0,00</span> <i>r</i></div>').insertBefore('#insertbefore2');
    }

    function leadZero(n) {
        return (n < 10 ? '0' : '') + n;
    };

    $('#minus').click(minus);
    $('#plus').click(plus);
    $('#addNewMatdem').click(addNewMatdem);

    if ($('#calculator').length) {
        document.forms['calculator'].onchange = calc;
        calc();
    }
}
